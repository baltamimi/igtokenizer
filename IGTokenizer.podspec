
Pod::Spec.new do |s|
  s.name             = 'IGTokenizer'
  s.version          = '0.2.0'
  s.summary          = 'A Tokenization helper for Intigral player video stream'

  s.homepage         = 'https://bitbucket.org/intigral'

  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Basil AlTamimi' => 'basil.altamimi@intigral.net' }
  s.source           = { :git => 'https://baltamimi@bitbucket.org/baltamimi/igtokenizer.git', :tag => s.version.to_s }


  s.ios.deployment_target = '8.0'

  s.source_files = 'IGTokenizer/**/*.{swift,m,h}'
  
  s.preserve_paths = 'CCommonCrypto/**/*'
  
  s.pod_target_xcconfig = {
    'SWIFT_INCLUDE_PATHS[sdk=iphoneos*]'         => '$(PODS_ROOT)/IGTokenizer/CCommonCrypto/iphoneos',
    'SWIFT_INCLUDE_PATHS[sdk=iphonesimulator*]'  => '$(PODS_ROOT)/IGTokenizer/CCommonCrypto/iphonesimulator',
    'SWIFT_VERSION' => '4.0'
  }

end
