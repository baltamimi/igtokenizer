//
//  IGCryptoRoutines.h
//  TokenGen
//
//  Created by Nabeel Ali Memon on 10/30/16.
//  Copyright © 2016 Intigral. All rights reserved.
//

#ifndef IGCryptoRoutines_h
#define IGCryptoRoutines_h


#endif /* IGCryptoRoutines_h */
#import <Foundation/Foundation.h>

@interface IGCryptoRoutines : NSObject

+(NSString*)hmac:(NSString*)plainText withKey:(NSString*)key usingAlgo:(int) algo;

@end
