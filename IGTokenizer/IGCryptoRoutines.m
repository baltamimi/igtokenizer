//
//  CryptoRoutine.m
//  TokenGen
//
//  Created by Nabeel Ali Memon on 10/30/16.
//  Copyright © 2016 Intigral. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IGCryptoRoutines.h"
#import <CommonCrypto/CommonCrypto.h>

#define MAX_FIELD_LEN 512
#define MAX_HASH_SOURCE 102400
#define MAX_TOKEN_LEN 102400

@implementation IGCryptoRoutines

+(NSString*)hmac:(NSString*)plainText withKey:(NSString*)key usingAlgo:(int)algo {
	const char* cKey  = [key cStringUsingEncoding:NSUTF8StringEncoding];
	const char* cData = [plainText cStringUsingEncoding:NSASCIIStringEncoding];
	
	unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
	
	char bin_key[MAX_FIELD_LEN];
	size_t bin_key_len = hexToBin(bin_key, (size_t)MAX_FIELD_LEN, cKey, strlen(cKey));
	
	CCHmac(algo, bin_key, bin_key_len, cData, strlen(cData), cHMAC);
	
	NSData *HMACData = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
	
	const unsigned char *buffer = (const unsigned char *)[HMACData bytes];
	NSString *HMAC = [NSMutableString stringWithCapacity:HMACData.length * 2];
	
	unsigned char hex_hmac[MAX_FIELD_LEN];
	binToHex(hex_hmac, MAX_FIELD_LEN, buffer, 32, 1);
	printf("hmac=%s\n", hex_hmac);
	for (int i = 0; i < HMACData.length; ++i)
		HMAC = [HMAC stringByAppendingFormat:@"%02lx", (unsigned long)buffer[i]];
	
	return HMAC;
}

size_t hexToBin(char* dest, size_t dest_len, const char* src, size_t src_len) {
	if (src_len % 2) {
		printf("You must have an even number of characters to convert from hex to binary.\n");
		exit(1);
	}
	if (dest_len < (src_len / 2)) {
		printf("Destination buffer is not large enough to hold binary data.\n");
		exit(1);
	}
	int i = 0;
	for( ; i < src_len ; i += 2) {
		char b1 = toupper(src[i]) - '0';
		char b2 = toupper(src[i+1]) - '0';
		if (b1 > 9) {
			b1 -= 7; // Adjust the value from A-F to 10-15
		}
		if (b2 > 9) {
			b2 -= 7; // Adjust the value from A-F to 10-15
		}
		dest[i/2] = (b1 << 4) + b2;
	}
	return (size_t)(src_len/2);
}

size_t binToHex(unsigned char* dest, size_t dest_len, const unsigned char* src, size_t src_len, int to_lower) {
	if (dest_len < ((src_len * 2)+1)) {
		printf("Destination buffer is not large enough to hold hex encoded source string.\n");
		exit(1);
	}
	int i = 0;
	for( ; i < src_len ; i++) {
		char b1 = (src[i] >> 4) + '0';
		char b2 = (src[i] & 0x0f) + '0';
		if (b1 > '9') {
			b1 += 7; // Offset to A-F for values greater than 9.
		}
		if (b2 > '9') {
			b2 += 7; // Offset to A-F for values greater than 9.
		}
		dest[(i*2)] = to_lower ? tolower(b1) : b1;
		dest[(i*2)+1] = to_lower ? tolower(b2) : b2;
	}
	dest[(src_len*2)+1] = '\0';
	return (size_t)(src_len * 2);
}

@end
