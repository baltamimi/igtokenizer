//
//  IGTokenizer.h
//  IGTokenizer
//
//  Created by Basel on 11/25/17.
//  Copyright © 2017 Intigral. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for IGTokenizer.
FOUNDATION_EXPORT double IGTokenizerVersionNumber;

//! Project version string for IGTokenizer.
FOUNDATION_EXPORT const unsigned char IGTokenizerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IGTokenizer/PublicHeader.h>

#import "IGCryptoRoutines.h"
