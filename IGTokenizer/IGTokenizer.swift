//
//  Tokenizer.swift
//  DawriPlus
//
//  Created by Uday Patial on 11/4/16.
//  Copyright © 2016 Intigral. All rights reserved.
//

import Foundation

public class IGTokenizer {
	private static let akamaiSecretKey = "5886B72A6413F63FB0D4BDCDE0D77A4F"
	private static let keyTTL = 86400
	public static func getTokenizedURL(_ tokenConfig: [String: String], akamaiVideoUrlBase: String) -> String {
		
		if let encryptionParams = IGTokenizedUrlGenerator().generate(tokenConfig){
			let tokenizedUrl = akamaiVideoUrlBase.replacingOccurrences(of: "{p1}", with: encryptionParams)
			//debugPrint("tokenized url is \(tokenizedUrl)")
			return tokenizedUrl
		} else {
			return akamaiVideoUrlBase
		}
	}
	
	public static func getTokenizedURL(_ ipAddress: String, streamUrl: String, tokenTTL: Int) -> String {
		let ip = ipAddress.trimmingCharacters(in: CharacterSet.newlines)
		return self.getTokenizedURL(["acl": "/*", "key": akamaiSecretKey, "window_seconds": String(tokenTTL), "ip_address": ip], akamaiVideoUrlBase: streamUrl)
	}
	
}





