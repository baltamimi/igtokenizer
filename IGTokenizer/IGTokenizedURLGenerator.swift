//
//  TokenizedURLGenerator.swift
//  TokenGen
//
//  Created by Nabeel Ali Memon on 10/30/16.
//  Copyright © 2016 Intigral. All rights reserved.
//

import Foundation
import CCommonCrypto

let programName = "akamai_token_v2"
let programVersion = "2.0.2"
let defaultTokenName = "hdnea"
let defaultAcl = "/*"
let defaultAlgo = "sha256"
let defaultFieldDelimiter = "~"
let defaultAclDelimiter = "!"

enum IGTokenError: Error {
	case unknownAlgorithm
	case startTimeNonNumeric
	case aclMissing
	case urlMissing
	case keyMissing
}

extension String {
	func hmacDigest(_ key: String, usingAlgo algo: Int) -> String {
		let digest = IGCryptoRoutines.hmac(self, withKey: key, usingAlgo: Int32(algo))
		return digest!
	}
}

struct IGTokenizedUrlGenerator {
	func generateToken(_ config: [String: String]) throws -> String {
		let algo = config["algo"] ?? defaultAlgo
		
		guard algo == "md5" || algo == "sha1" || algo == "sha256" else {
			throw IGTokenError.unknownAlgorithm
		}
		
		
		let startTimeText = config["start_time"] ?? ""
		var startTime = Int64(0)
		if startTimeText.lowercased() == "now" {
			let utcTZ = TimeZone(abbreviation: "UTC")!
			var cal = Calendar.current
			cal.timeZone = utcTZ
			startTime = Int64(cal.date(from: cal.dateComponents(in: utcTZ, from: Date()))!.timeIntervalSince1970)
		} else if !startTimeText.isEmpty {
			startTime = Int64(startTimeText)!
		}
		
		let window = Int64(config["window_seconds"] ?? "0")!
		
		let endTimeText = config["end_time"] ?? ""
		var endTime = Int64(0)
		if endTimeText.lowercased() == "now" {
			let utcTZ = TimeZone(abbreviation: "UTC")!
			var cal = Calendar.current
			cal.timeZone = utcTZ
			endTime = Int64(cal.date(from: cal.dateComponents(in: utcTZ, from: Date()))!.timeIntervalSince1970)
		} else {
			if startTimeText.lowercased() != "" {
				endTime = startTime + window
			} else {
				let utcTZ = TimeZone(abbreviation: "UTC")!
				var cal = Calendar.current
				cal.timeZone = utcTZ
				endTime = Int64(cal.date(from: cal.dateComponents(in: utcTZ, from: Date()))!.timeIntervalSince1970) + window
			}
		}
		
		let acl = config["acl"] ?? ""
		let url = config["url"] ?? ""
		
		if acl.isEmpty && url.isEmpty {
			throw IGTokenError.urlMissing
		}
		
		guard let key = config["key"], !key.isEmpty else {
			throw IGTokenError.keyMissing
		}
		
		if let verbose = config["verbose"], verbose.lowercased() == "true" {
			displayParameters(config)
		}
		
		let newToken = String("\(getTokenIP(config))\(getTokenStartTime(config))exp=\(endTime)\(getFieldDelimiter(config))\(getTokenAcl(config))\(getTokenSessionId(config))\(getTokenPayload(config))".dropLast())
		
		let hashSource = "\(newToken)\(getTokenUrl(config))\(getTokenSalt(config))"
		
		var cryptoAlgo: Int = kCCHmacAlgSHA256
		if algo == "sha1" {
			cryptoAlgo = kCCHmacAlgSHA1
		} else if algo == "md5" {
			cryptoAlgo = kCCHmacAlgMD5
		}
		
		let digestUrl = "\(config["token_name"] ?? defaultTokenName)=\(newToken)~hmac=\(hashSource.hmacDigest(config["key"] ?? "", usingAlgo: cryptoAlgo))"
		
		debugPrint("generated tokenized url str: \(digestUrl)")
		
		return digestUrl
	}
	
	func strFrom(_ result: [Int], length: Int) -> String {
		let hash = NSMutableString()
		for i in 0..<length {
			hash.appendFormat("%02x", result[i])
		}
		return String(hash)
	}
	
	func getFieldDelimiter(_ config: [String: String]) -> String {
		return config["field_delimiter"] ?? defaultFieldDelimiter
	}
	
	func getTokenIP(_ config: [String: String]) -> String {
		if let ip = config["ip_address"] {
			return "ip=\(ip)\(getFieldDelimiter(config))"
		}
		return ""
	}
	
	func getTokenStartTime(_ config: [String: String]) -> String {
		if let startTime = config["start_time"] {
			return "st=\(startTime)\(getFieldDelimiter(config))"
		}
		return ""
	}
	
	func getTokenEndTime(_ config: [String: String]) -> String {
		return "exp=\(config["end_time"] ?? "")\(getFieldDelimiter(config))"
	}
	
	func getTokenAcl(_ config: [String: String]) -> String {
		return "acl=\(config["acl"] ?? "")\(getFieldDelimiter(config))"
	}
	
	func getTokenSessionId(_ config: [String: String]) -> String {
		if let sessionId = config["session_id"] {
			return "id=\(sessionId)\(getFieldDelimiter(config))"
		}
		return ""
	}
	
	func getTokenPayload(_ config: [String: String]) -> String {
		if let payload = config["payload"] {
			return "data=\(payload)\(getFieldDelimiter(config))"
		}
		return ""
	}
	
	func getTokenUrl(_ config: [String: String]) -> String {
		if let url = config["url"] {
			return "url=\(url)\(getFieldDelimiter(config))"
		}
		return ""
	}
	
	func getTokenSalt(_ config: [String: String]) -> String {
		if let salt = config["salt"] {
			return "salt=\(salt)\(getFieldDelimiter(config))"
		}
		return ""
	}
	
	/*func escapeEarly(config: [String: String], text: String) -> String {
	let escapeEarly = config["escape_early"] ?? "false"
	let escapeEarlyUpper = config["escape_early_upper"] ?? "false"
	let newText =
	}*/
	
	enum HMACAlgorithm: String {
		case MD5 = "HmacMD5", SHA1 = "HmacSHA1", SHA256 = "HmacSHA256"
		
		func toCCHmacAlgorithm() -> CCHmacAlgorithm {
			var result: Int = 0
			switch self {
			case .MD5:
				result = kCCHmacAlgMD5
			case .SHA1:
				result = kCCHmacAlgSHA1
			case .SHA256:
				result = kCCHmacAlgSHA256
			}
			return CCHmacAlgorithm(result)
		}
		
		func digestLength() -> Int {
			var result: CInt = 0
			switch self {
			case .MD5:
				result = CC_MD5_DIGEST_LENGTH
			case .SHA1:
				result = CC_SHA1_DIGEST_LENGTH
			case .SHA256:
				result = CC_SHA256_DIGEST_LENGTH
			}
			return Int(result)
		}
	}
	
	func displayVersion() {
		debugPrint(programVersion)
	}
	
	func displayHelp() {
		debugPrint("Usage: swift AkamaiToken [options]")
		debugPrint("i.e.")
		debugPrint("swift AkamaiToken")
		debugPrint("")
		debugPrint("Options:")
		debugPrint("  --version             show program's version number and exit")
		debugPrint("  -h, --help            show this help message and exit")
		debugPrint("  -t TOKEN_TYPE, --token_type TOKEN_TYPE")
		debugPrint("                        Select a preset: (Not Supported Yet) [2.0, 2.0.2 ,PV, Debug]")
		debugPrint("  -n TOKEN_NAME, --token_name TOKEN_NAME")
		debugPrint("                        Parameter name for the new token. [Default:hdnts]")
		debugPrint("  -i IP_ADDRESS, --ip IP_ADDRESS")
		debugPrint("                        IP Address to restrict this token to.")
		debugPrint("  -s START_TIME, --start_time START_TIME")
		debugPrint("                        What is the start time. (Use now for the current time)")
		debugPrint("  -e END_TIME, --end_time END_TIME")
		debugPrint("                        When does this token expire? --end_time overrides")
		debugPrint("                        --window [Used for:URL or COOKIE]")
		debugPrint("  -w WINDOW_SECONDS, --window WINDOW_SECONDS")
		debugPrint("                        How long is this token valid for?")
		debugPrint("  -u URL, --url URL     URL path. [Used for:URL]")
		debugPrint("  -a ACCESS_LIST, --acl ACCESS_LIST")
		debugPrint("                        Access control list delimited by ! [ie. /*]")
		debugPrint("  -k KEY, --key KEY     Secret required to generate the token.")
		debugPrint("  -p PAYLOAD, --payload PAYLOAD")
		debugPrint("                        Additional text added to the calculated digest.")
		debugPrint("  -A ALGORITHM, --algo ALGORITHM")
		debugPrint("                        Algorithm to use to generate the token. (sha1, sha256,")
		debugPrint("                        or md5) [Default:sha256]")
		debugPrint("  -S SALT, --salt SALT  Additional data validated by the token but NOT")
		debugPrint("                        included in the token body.")
		debugPrint("  -I SESSION_ID, --session_id SESSION_ID")
		debugPrint("                        The session identifier for single use tokens or other")
		debugPrint("                        advanced cases.")
		debugPrint("  -d FIELD_DELIMITER, --field_delimiter FIELD_DELIMITER")
		debugPrint("                        Character used to delimit token body fields.")
		debugPrint("                        [Default:~]")
		debugPrint("  -D ACL_DELIMITER, --acl_delimiter ACL_DELIMITER")
		debugPrint("                        Character used to delimit acl fields. [Default:!]")
		debugPrint("  -x, --escape_early    Causes strings to be url encoded before being used.")
		debugPrint("                        (legacy 2.0 behavior)")
		debugPrint("  -X, --escape_early_upper")
		debugPrint("                        Causes strings to be url encoded before being used.")
		debugPrint("                        (legacy 2.0 behavior)")
		debugPrint("  -v, --verbose")
	}
	
	func displayParameters(_ config: [String: String]) {
		debugPrint("Akamai Token Generation Parameters")
		debugPrint("    Token Type      : \(config["token_type"] ?? "")")
		debugPrint("    Token Name      : \(config["token_name"] ?? defaultTokenName)")
		debugPrint("    Start Time      : \(config["start_time"] ?? "")")
		debugPrint("    Window(seconds) : \(config["window_seconds"] ?? "")")
		debugPrint("    End Time        : \(config["end_time"] ?? "")")
		debugPrint("    IP              : \(config["ip_address"] ?? "")")
		debugPrint("    URL             : \(config["url"] ?? "")")
		debugPrint("    ACL             : \(config["acl"] ?? defaultAcl)")
		debugPrint("    Key/Secret      : \(config["key"] ?? "")")
		debugPrint("    Payload         : \(config["payload"] ?? "")")
		debugPrint("    Algo            : \(config["algo"] ?? defaultAlgo)")
		debugPrint("    Salt            : \(config["salt"] ?? "")")
		debugPrint("    Session ID      : \(config["session_id"] ?? "")")
		debugPrint("    Field Delimiter : \(getFieldDelimiter(config))")
		debugPrint("    ACL Delimiter   : \(config["acl_delimiter"] ?? defaultAclDelimiter)")
		debugPrint("Generating token...")
	}
	
	
	func generate(_ config: [String: String]) -> String? {
		do {
			//"ip_address": "94.200.42.194",
			let tokenizedUrl = try generateToken(config)
			//let playableUrl = "\(tokenizedUrl)&g=TMUKEWUTKBBV"
			let playableUrl = "\(tokenizedUrl)"
			return playableUrl
		} catch let e {
			debugPrint("error: \(e)")
			return nil
		}
	}
	
	func execExample() -> String? {
		return generate(["acl": "/*", "key": "5886B72A6413F63FB0D4BDCDE0D77A4F", "window_seconds": "300"])
	}
}
